package com.example.sqliteoperations;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText Name, X_Coordinate, Y_Coordinate , updateold, updatenew, delete;
    myDbAdapter helper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Name= findViewById(R.id.editName);
        X_Coordinate= findViewById(R.id.editX_Coordinate);
        Y_Coordinate= findViewById(R.id.edit_Y_Coordinate);
        updateold= findViewById(R.id.editText3);
        updatenew= findViewById(R.id.editText5);
        delete = findViewById(R.id.editText6);

        helper = new myDbAdapter(this);
    }

    public void addPlace(View view)
    {
        String t1 = Name.getText().toString();
        String t2 = Y_Coordinate.getText().toString();
        String t3 = X_Coordinate.getText().toString();
        if(t1.isEmpty() || t2.isEmpty() || t3.isEmpty())
        {
            Message.message(getApplicationContext(),"Enter Both Name and Coordinates");
        }
        else
        {
            long id = helper.insertData(t1,t2,t3);
            if(id<=0)
            {
                Message.message(getApplicationContext(),"Insertion Unsuccessful");
                Name.setText("");
                Y_Coordinate.setText("");
                X_Coordinate.setText("");
            } else
            {
                Message.message(getApplicationContext(),"Insertion Successful");
                Name.setText("");
                Y_Coordinate.setText("");
                X_Coordinate.setText("");
            }
        }
    }

    public void viewdata(View view)
    {
        String data = helper.getData();
        Message.message(this,data);
    }

    public void update( View view)
    {
        String u1 = updateold.getText().toString();
        String u2 = updatenew.getText().toString();
        if(u1.isEmpty() || u2.isEmpty())
        {
            Message.message(getApplicationContext(),"Enter Data");
        }
        else
        {
            int a= helper.updateName( u1, u2);
            if(a<=0)
            {
                Message.message(getApplicationContext(),"Unsuccessful");
                updateold.setText("");
                updatenew.setText("");
            } else {
                Message.message(getApplicationContext(),"Updated");
                updateold.setText("");
                updatenew.setText("");
            }
        }

    }
    public void delete( View view)
    {
        String uname = delete.getText().toString();
        if(uname.isEmpty())
        {
            Message.message(getApplicationContext(),"Enter Data");
        }
        else{
            int a= helper.delete(uname);
            if(a<=0)
            {
                Message.message(getApplicationContext(),"Unsuccessful");
                delete.setText("");
            }
            else
            {
                Message.message(this, "DELETED");
                delete.setText("");
            }
        }
    }
}